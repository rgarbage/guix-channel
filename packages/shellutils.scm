(define-module (packages shellutils)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module (gnu packages base)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages ncurses))

(define-public liquidprompt
  (package
    (name "liquidprompt")
    (version "2.1.2")
    (home-page "https://github.com/liquidprompt/liquidprompt")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/liquidprompt/liquidprompt")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0ljlq97mh84d6g6r3abb254vrwrdan5v74b69fpd62d7p9ffnsgf"))))
    (build-system copy-build-system)
    (inputs (list ncurses))
    (arguments
     (list
      #:install-plan #~'(("liquidpromptrc-dist" "etc/liquidpromptrc")
                         ("example.bashrc" "share/liquidprompt/examples/")
                         ("liquid.ps1" "share/liquidprompt/examples/")
                         ("liquidprompt" "share/liquidprompt/")
                         ("contrib" "share/liquidprompt/")
                         ("themes" "share/liquidprompt/")
                         ("liquidprompt.plugin.zsh"
                          "share/zsh/plugins/liquidprompt/")
                         ("docs" #$(string-append "share/doc/" name "-"
                                                  version "/")))
      #:phases #~(modify-phases %standard-phases
                   (add-after 'unpack 'fix-plugin
                     (lambda _
                       (substitute* "liquidprompt.plugin.zsh"
                         (("source(.*)$")
                          (string-append "source "
                                         #$output
                                         "/share/liquidprompt/liquidprompt")))))
		   (add-after 'fix-plugin 'fix-tput-path
		     (lambda _
		       (substitute* "liquidprompt"
			 (("([ (])tput " all beg) (string-append beg #$ncurses "/bin/tput "))))))))
    (synopsis "Full-featured prompt for Bash & Zsh")
    (description
     "Liquidprompt is an adaptive prompt for Bash and Zsh that gives
you a nicely displayed prompt with useful information when you need it. It
does this with a powerful theming engine and a large array of data sources.

In order to use liquidprompt with Zsh, you should use the following snippet
with @code{guix home}:
@example
(services (list ;;...
            (service home-zsh-service-type
                     (home-zsh-configuration
                       (zshrc (list ;;...
                                ;; This loads liquidprompt
                                (mixed-text-file \"liquidprompt\"
                                                 \"[[ $- = *i* ]] && source \" liquidprompt \"/share/liquidprompt/liquidprompt\")
                                ;; This loads the powerline theme available in liquidprompt
                                (mixed-text-file \"powerline-theme\"
                                                 \"source \" liquidprompt \"/share/liquidprompt/themes/powerline/powerline.theme\")))))))
@end example
")
    (license license:agpl3)))
