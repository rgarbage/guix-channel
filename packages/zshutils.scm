(define-module (packages zshutils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy))

(define-public zsh-vcs-prompt
  (let ((commit "68d68a577ccb6545677f32db1b2505ea04a2ef7d")
        (revision "1"))
    (package
      (name "zsh-vcs-prompt")
      (version (git-version "1.1" revision commit))
      (home-page "https://github.com/yonchu/zsh-vcs-prompt")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                       (url "https://github.com/yonchu/zsh-vcs-prompt")
                       (commit commit)))
                (sha256 (base32 "0gx44dnwkyx15j1s08p0l6ai7h2cmwjshgs2hgfdfjkbs25jg90x"))
                (file-name (git-file-name name version))))
      (build-system copy-build-system)
      (arguments
        '(#:install-plan '(("." "/share/zsh/plugins/zsh-vcs-prompt"))))
      (synopsis "Zsh prompt that displays VCS related information")
      (description "A zsh prompt that displays information about the current
vcs (git/svn/hg) repository. In particular the branch name, difference with
remote branch, number of files staged, changed, etc.")
      (license license:giftware))))
zsh-vcs-prompt
