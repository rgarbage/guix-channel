(define-module (packages emacs-xyz)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (gnu packages emacs-xyz))

(define-public emacs-forge-custom
  (package/inherit emacs-forge
    (name "emacs-forge")
    (version "0.3.2a")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/rgarbage/magit-forge")
             (commit "edf0559e19d40a9f4277bacc71d78ed2345713bd")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "188kqmr0qqlwdw69l7k15am5csn6i2kr5jd5kfjdl8drb8g6pyyv"))))))
