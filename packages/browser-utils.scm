(define-module (packages browser-utils)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix build gnu-build-system)
  #:use-module (guix build-system copy)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages sqlite))

(define-public mattermost-session-cookie-firefox
  (let ((commit "6de19baf4017393a9693f7183a194d89c4a3204f")
        (version "0.1")
        (revision "1"))
    (package
     (name "mattermost-session-cookie-firefox")
     (version (git-version version revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/ftilde/mattermost-session-cookie-firefox")
             (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "013iizmjl1j1hqmmf4l7vair6vlm2qx3wmigyp91pn351r1gsyy2"))))
     (build-system copy-build-system)
     (inputs (list curl sqlite))
     (arguments
      (list
       #:install-plan #~'((#$name "bin/mattermost-session-cookie"))
       #:phases #~(modify-phases
                   %standard-phases
                   (add-after 'unpack 'fix-ff-path
                              (lambda _
                                (substitute* "mattermost-session-cookie-firefox"
                                             (("(firefox_dir=\\\"\\$HOME/.mozilla/)firefox" all beg)
                                              (string-append beg "icecat")))))
                   (add-after 'unpack 'fix-script
                              (lambda _
                                (substitute* "mattermost-session-cookie-firefox"
                                             (("sqlite3")
                                              (string-append #$sqlite "/bin/sqlite3"))
                                             (("curl")
                                              (string-append #$curl "/bin/curl"))))))))
     (home-page "https://github.com/ftilde/mattermost-session-cookie-firefox")
     (synopsis
      "Obtain the session cookie from a mattermost webclient for use in other applications")
     (description "@cmd{mattermost-session-cookie-firefox} is a script to obtain the session
cookie from a mattermost webclient. It is designed to be used with the
tokencmd configuration option of matterhorn for mattermost instances
that do not allow username/password or personal access token login.")
     (license license:expat))))
