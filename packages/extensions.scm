(define-module (packages extensions)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix build-system copy)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download))

(define-public guix-extensions
  (package
    (name "guix-extensions")
    (version "0.0.1")
    (home-page "https://gitlab.inria.fr/rgarbage/guix-extensions")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://gitlab.inria.fr/rgarbage/guix-extensions")
	     (commit (string-append "v" version)))
	    )
       (file-name (git-file-name name version))
       (sha256
	(base32 "0gqjnsn22yabkz0bx94iq1fsg7bm6xhh581yr1gm9cmarhgd6fmd"))
       ))
    (build-system copy-build-system)
    (synopsis "Extensions for the @command{guix} command")
    (description "This package extends the @command{guix} command with
the following subcommands:
 - @command{list}: list the dependencies (or reverse dependencies) of a package")
    (license license:gpl3+)))
