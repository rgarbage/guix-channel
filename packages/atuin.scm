(define-module (packages atuin)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix build-system cargo)
  #:use-module (gnu packages crates-apple)
  #:use-module (gnu packages crates-crypto)
  #:use-module (gnu packages crates-database)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-shell)
  #:use-module (gnu packages crates-tls)
  #:use-module (gnu packages crates-vcs)
  #:use-module (gnu packages crates-web)
  #:use-module (gnu packages crates-windows)
  #:use-module (gnu packages shells))

(define-public rust-tracing-tree-0.4
  (package
    (name "rust-tracing-tree")
    (version "0.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tracing-tree" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "175lqyfp6zq7jbj8m026xdp8p765pzgfdzfxahfggmdhy5wwlngl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-nu-ansi-term" ,rust-nu-ansi-term-0.50)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tracing-core" ,rust-tracing-core-0.1)
                       ("rust-tracing-log" ,rust-tracing-log-0.2)
                       ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.3))))
    (home-page "https://github.com/davidbarsky/tracing-tree")
    (synopsis "Tracing Layer which prints a tree of spans and events.")
    (description
     "This package provides a Tracing Layer which prints a tree of spans and events.")
    (license (list license:expat license:asl2.0))))

(define-public rust-runtime-format-0.1
  (package
    (name "rust-runtime-format")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "runtime-format" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "154c7jq7kbpc5acn2ysa2ilab2x0i5y7d34jwznni9xw71dqv589"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-tinyvec" ,rust-tinyvec-1))))
    (home-page "https://github.com/conradludgate/strfmt")
    (synopsis "rust library for formatting dynamic strings")
    (description
     "This package provides rust library for formatting dynamic strings.")
    (license license:expat)))

(define-public rust-atuin-server-postgres-18
  (package
    (name "rust-atuin-server-postgres")
    (version "18.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "atuin-server-postgres" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0k4bv7fzzpdh1yl2bg5b04gp0mlkb861x545wp8gggijk280ih99"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-async-trait" ,rust-async-trait-0.1)
                       ("rust-atuin-common" ,rust-atuin-common-18)
                       ("rust-atuin-server-database" ,rust-atuin-server-database-18)
                       ("rust-eyre" ,rust-eyre-0.6)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-metrics" ,rust-metrics-0.21)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-sqlx" ,rust-sqlx-0.8)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-url" ,rust-url-2)
                       ("rust-uuid" ,rust-uuid-1))))
    (home-page "https://atuin.sh")
    (synopsis "server postgres database library for atuin")
    (description
     "This package provides server postgres database library for atuin.")
    (license license:expat)))

(define-public rust-postmark-0.10
  (package
    (name "rust-postmark")
    (version "0.10.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "postmark" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "10vd1xdlk189p8qphmihm9j28wdn5fclcgwc6z65fs43i4irihd8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-async-trait" ,rust-async-trait-0.1)
                       ("rust-bytes" ,rust-bytes-1)
                       ("rust-http" ,rust-http-1)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-reqwest" ,rust-reqwest-0.12)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-typed-builder" ,rust-typed-builder-0.18)
                       ("rust-url" ,rust-url-2))))
    (home-page "https://github.com/pastjean/postmark-rs")
    (synopsis "Postmark rust client")
    (description "This package provides Postmark rust client.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sketches-ddsketch-0.2
  (package
    (name "rust-sketches-ddsketch")
    (version "0.2.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sketches-ddsketch" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0p6n1v0p0773d0b5qnsnw526g7hhlb08bx95wm0zb09xnwa6qqw5"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/mheffner/rust-sketches-ddsketch")
    (synopsis "direct port of the Golang DDSketch implementation.")
    (description
     "This package provides a direct port of the Golang DDSketch implementation.")
    (license license:asl2.0)))

(define-public rust-prost-types-0.11
  (package
    (name "rust-prost-types")
    (version "0.11.9")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "prost-types" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "04ryk38sqkp2nf4dgdqdfbgn6zwwvjraw6hqq6d9a6088shj4di1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-prost" ,rust-prost-0.11))))
    (home-page "https://github.com/tokio-rs/prost")
    (synopsis "Prost definitions of Protocol Buffers well known types")
    (description
     "This package provides Prost definitions of Protocol Buffers well known types.")
    (license license:asl2.0)))

(define-public rust-quanta-0.11
  (package
    (name "rust-quanta")
    (version "0.11.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "quanta" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1axrw0nqc90bq671w05jd9460pmwg86c4r132mjsi4c2g8m6czm1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-crossbeam-utils" ,rust-crossbeam-utils-0.8)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-mach2" ,rust-mach2-0.4)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-prost-types" ,rust-prost-types-0.11)
                       ("rust-raw-cpuid" ,rust-raw-cpuid-10)
                       ("rust-wasi" ,rust-wasi-0.11)
                       ("rust-web-sys" ,rust-web-sys-0.3)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/metrics-rs/quanta")
    (synopsis "high-speed timing library")
    (description "This package provides high-speed timing library.")
    (license license:expat)))

(define-public rust-hashbrown-0.13
  (package
    (name "rust-hashbrown")
    (version "0.13.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "hashbrown" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0f602rk7pgdhw1s57g81822g7b2m5i2wibrpaqp11afk5kk8mzrk"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-ahash" ,rust-ahash-0.8)
                       ("rust-bumpalo" ,rust-bumpalo-3)
                       ("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-rustc-std-workspace-alloc" ,rust-rustc-std-workspace-alloc-1)
                       ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/rust-lang/hashbrown")
    (synopsis "Rust port of Google's SwissTable hash map")
    (description
     "This package provides a Rust port of Google's @code{SwissTable} hash map.")
    (license (list license:expat license:asl2.0))))

(define-public rust-metrics-util-0.15
  (package
    (name "rust-metrics-util")
    (version "0.15.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "metrics-util" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0glpkmrj7zkg9b290x6qxf93kmd9b4b4sbkk1fs19l8y95pfvqjd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-ahash" ,rust-ahash-0.8)
                       ("rust-aho-corasick" ,rust-aho-corasick-1)
                       ("rust-crossbeam-epoch" ,rust-crossbeam-epoch-0.9)
                       ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.8)
                       ("rust-hashbrown" ,rust-hashbrown-0.13)
                       ("rust-indexmap" ,rust-indexmap-1)
                       ("rust-metrics" ,rust-metrics-0.21)
                       ("rust-num-cpus" ,rust-num-cpus-1)
                       ("rust-ordered-float" ,rust-ordered-float-3)
                       ("rust-quanta" ,rust-quanta-0.11)
                       ("rust-radix-trie" ,rust-radix-trie-0.2)
                       ("rust-sketches-ddsketch" ,rust-sketches-ddsketch-0.2))))
    (home-page "https://github.com/metrics-rs/metrics")
    (synopsis "Helper types/functions used by the metrics ecosystem")
    (description
     "This package provides Helper types/functions used by the metrics ecosystem.")
    (license license:expat)))

(define-public rust-metrics-exporter-prometheus-0.12
  (package
    (name "rust-metrics-exporter-prometheus")
    (version "0.12.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "metrics-exporter-prometheus" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0l19s21jfmwm72cxfjq35xb79a5wi4fv7c1p993dnqj8gk7afkqx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-base64" ,rust-base64-0.21)
                       ("rust-hyper" ,rust-hyper-0.14)
                       ("rust-indexmap" ,rust-indexmap-1)
                       ("rust-ipnet" ,rust-ipnet-2)
                       ("rust-metrics" ,rust-metrics-0.21)
                       ("rust-metrics-util" ,rust-metrics-util-0.15)
                       ("rust-quanta" ,rust-quanta-0.11)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://github.com/metrics-rs/metrics")
    (synopsis "metrics-compatible exporter for sending metrics to Prometheus.")
    (description
     "This package provides a metrics-compatible exporter for sending metrics to
Prometheus.")
    (license license:expat)))

(define-public rust-metrics-macros-0.7
  (package
    (name "rust-metrics-macros")
    (version "0.7.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "metrics-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0krmj7zyr4g14jdpk1jasi1w2nw64hqdxb2lfx4zxphp0vqgmd1q"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/metrics-rs/metrics")
    (synopsis "Macros for the metrics crate")
    (description "This package provides Macros for the metrics crate.")
    (license license:expat)))

(define-public rust-metrics-0.21
  (package
    (name "rust-metrics")
    (version "0.21.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "metrics" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1ibndxzk0sja8cgwrr73b9vzbgfvwzwxwkxqiivnmmwy00dazqzx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-ahash" ,rust-ahash-0.8)
                       ("rust-metrics-macros" ,rust-metrics-macros-0.7)
                       ("rust-portable-atomic" ,rust-portable-atomic-1))))
    (home-page "https://github.com/metrics-rs/metrics")
    (synopsis "lightweight metrics facade.")
    (description "This package provides a lightweight metrics facade.")
    (license license:expat)))

(define-public rust-axum-server-0.7
  (package
    (name "rust-axum-server")
    (version "0.7.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "axum-server" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1n67cx39cm9zsm0dwm0nla67qjswj90ccqrwq0x3kagn904ckfjn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-arc-swap" ,rust-arc-swap-1)
                       ("rust-bytes" ,rust-bytes-1)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-http" ,rust-http-1)
                       ("rust-http-body" ,rust-http-body-1)
                       ("rust-http-body-util" ,rust-http-body-util-0.1)
                       ("rust-hyper" ,rust-hyper-1)
                       ("rust-hyper-util" ,rust-hyper-util-0.1)
                       ("rust-openssl" ,rust-openssl-0.10)
                       ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
                       ("rust-rustls" ,rust-rustls-0.23)
                       ("rust-rustls-pemfile" ,rust-rustls-pemfile-2)
                       ("rust-rustls-pki-types" ,rust-rustls-pki-types-1)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tokio-openssl" ,rust-tokio-openssl-0.6)
                       ("rust-tokio-rustls" ,rust-tokio-rustls-0.26)
                       ("rust-tower" ,rust-tower-0.4)
                       ("rust-tower-service" ,rust-tower-service-0.3))))
    (home-page "https://github.com/programatik29/axum-server")
    (synopsis "High level server designed to be used with axum framework")
    (description
     "This package provides High level server designed to be used with axum framework.")
    (license license:expat)))

(define-public rust-atuin-server-database-18
  (package
    (name "rust-atuin-server-database")
    (version "18.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "atuin-server-database" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "08g1pim38jpixa6d13z5vcir9y7f85a2799w7rwm8r19b24y5yhz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-async-trait" ,rust-async-trait-0.1)
                       ("rust-atuin-common" ,rust-atuin-common-18)
                       ("rust-eyre" ,rust-eyre-0.6)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-uuid" ,rust-uuid-1))))
    (home-page "https://atuin.sh")
    (synopsis "server database library for atuin")
    (description "This package provides server database library for atuin.")
    (license license:expat)))

(define-public rust-atuin-server-18
  (package
    (name "rust-atuin-server")
    (version "18.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "atuin-server" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "15p074z5ng24ln89bwcvicw3zvp140aily5aclyydlsfr81l8w5a"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-argon2" ,rust-argon2-0.5)
                       ("rust-async-trait" ,rust-async-trait-0.1)
                       ("rust-atuin-common" ,rust-atuin-common-18)
                       ("rust-atuin-server-database" ,rust-atuin-server-database-18)
                       ("rust-axum" ,rust-axum-0.7)
                       ("rust-axum-server" ,rust-axum-server-0.7)
                       ("rust-base64" ,rust-base64-0.22)
                       ("rust-config" ,rust-config-0.13)
                       ("rust-eyre" ,rust-eyre-0.6)
                       ("rust-fs-err" ,rust-fs-err-2)
                       ("rust-metrics" ,rust-metrics-0.21)
                       ("rust-metrics-exporter-prometheus" ,rust-metrics-exporter-prometheus-0.12)
                       ("rust-postmark" ,rust-postmark-0.10)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-reqwest" ,rust-reqwest-0.11)
                       ("rust-rustls" ,rust-rustls-0.23)
                       ("rust-rustls-pemfile" ,rust-rustls-pemfile-2)
                       ("rust-semver" ,rust-semver-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tower" ,rust-tower-0.4)
                       ("rust-tower-http" ,rust-tower-http-0.5)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-uuid" ,rust-uuid-1))))
    (home-page "https://atuin.sh")
    (synopsis "server library for atuin")
    (description "This package provides server library for atuin.")
    (license license:expat)))

(define-public rust-tonic-types-0.12
  (package
    (name "rust-tonic-types")
    (version "0.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tonic-types" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0rxkz100jaiqlr47dim69mfhyq54c3lynnia75qi5l2713pdi080"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-prost" ,rust-prost-0.13)
                       ("rust-prost-types" ,rust-prost-types-0.13)
                       ("rust-tonic" ,rust-tonic-0.12))))
    (home-page "https://github.com/hyperium/tonic")
    (synopsis
     "collection of useful protobuf types that can be used with `tonic`.")
    (description
     "This package provides a collection of useful protobuf types that can be used
with `tonic`.")
    (license license:expat)))

(define-public rust-protox-parse-0.7
  (package
    (name "rust-protox-parse")
    (version "0.7.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "protox-parse" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1pld0s1cg9favgy9bafkwlvmg65ky13rmhh0w050hb262p8n5953"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-logos" ,rust-logos-0.14)
                       ("rust-miette" ,rust-miette-7)
                       ("rust-prost-types" ,rust-prost-types-0.13)
                       ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/andrewhickman/protox")
    (synopsis "Parsing of protobuf source files")
    (description "This package provides Parsing of protobuf source files.")
    (license (list license:expat license:asl2.0))))

(define-public rust-prost-reflect-derive-0.14
  (package
    (name "rust-prost-reflect-derive")
    (version "0.14.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "prost-reflect-derive" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0rfbgs03m05w2pcdvnf7asyk6ar056nrhaqa826qvk0m5yrfdz7l"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/andrewhickman/prost-reflect")
    (synopsis
     "derive macro for prost-reflect to generate ReflectMessage implementations")
    (description
     "This package provides a derive macro for prost-reflect to generate
@code{ReflectMessage} implementations.")
    (license (list license:expat license:asl2.0))))

(define-public rust-prost-reflect-0.14
  (package
    (name "rust-prost-reflect")
    (version "0.14.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "prost-reflect" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0npvpgfnmgc0fxhyldl3kc2rg2mw91dvbgnhl4nkwnp04jfraaz9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-base64" ,rust-base64-0.22)
                       ("rust-logos" ,rust-logos-0.14)
                       ("rust-miette" ,rust-miette-7)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-prost" ,rust-prost-0.13)
                       ("rust-prost-reflect-derive" ,rust-prost-reflect-derive-0.14)
                       ("rust-prost-types" ,rust-prost-types-0.13)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-value" ,rust-serde-value-0.7))))
    (home-page "https://github.com/andrewhickman/prost-reflect")
    (synopsis
     "protobuf library extending prost with reflection support and dynamic messages.")
    (description
     "This package provides a protobuf library extending prost with reflection support
and dynamic messages.")
    (license (list license:expat license:asl2.0))))

(define-public rust-protox-0.7
  (package
    (name "rust-protox")
    (version "0.7.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "protox" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0jmmcil88n15kdpac51fz9qjagchpy3pq3vjrj77nqxz67rjldbg"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bytes" ,rust-bytes-1)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-miette" ,rust-miette-7)
                       ("rust-prost" ,rust-prost-0.13)
                       ("rust-prost-reflect" ,rust-prost-reflect-0.14)
                       ("rust-prost-types" ,rust-prost-types-0.13)
                       ("rust-protox-parse" ,rust-protox-parse-0.7)
                       ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/andrewhickman/protox")
    (synopsis "rust implementation of the protobuf compiler.")
    (description
     "This package provides a rust implementation of the protobuf compiler.")
    (license (list license:expat license:asl2.0))))

(define-public rust-atuin-history-18
  (package
    (name "rust-atuin-history")
    (version "18.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "atuin-history" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1qy0x84cjjrj5ix9x5gxbdn042drrrr9v2d02dl8fz07w60rbmz5"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-async-trait" ,rust-async-trait-0.1)
                       ("rust-atuin-client" ,rust-atuin-client-18)
                       ("rust-atuin-common" ,rust-atuin-common-18)
                       ("rust-base64" ,rust-base64-0.22)
                       ("rust-crossterm" ,rust-crossterm-0.27)
                       ("rust-directories" ,rust-directories-5)
                       ("rust-eyre" ,rust-eyre-0.6)
                       ("rust-fs-err" ,rust-fs-err-2)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-indicatif" ,rust-indicatif-0.17)
                       ("rust-interim" ,rust-interim-0.1)
                       ("rust-itertools" ,rust-itertools-0.13)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-semver" ,rust-semver-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-sysinfo" ,rust-sysinfo-0.30)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
                       ("rust-unicode-width" ,rust-unicode-width-0.1)
                       ("rust-uuid" ,rust-uuid-1)
                       ("rust-whoami" ,rust-whoami-1))))
    (home-page "https://atuin.sh")
    (synopsis "The history crate for Atuin")
    (description "This package provides The history crate for Atuin.")
    (license license:expat)))

(define-public rust-atuin-dotfiles-18
  (package
    (name "rust-atuin-dotfiles")
    (version "18.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "atuin-dotfiles" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0adln7gfrr38zy9slxi33k0ma4a0iabz5jqpv8anrag2fab0951k"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-atuin-client" ,rust-atuin-client-18)
                       ("rust-atuin-common" ,rust-atuin-common-18)
                       ("rust-crypto-secretbox" ,rust-crypto-secretbox-0.1)
                       ("rust-eyre" ,rust-eyre-0.6)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rmp" ,rust-rmp-0.8)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://atuin.sh")
    (synopsis "The dotfiles crate for Atuin")
    (description "This package provides The dotfiles crate for Atuin.")
    (license license:expat)))

(define-public rust-atuin-daemon-18
  (package
    (name "rust-atuin-daemon")
    (version "18.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "atuin-daemon" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0wlyrpjm30khd7m51z6xfiny2wc63hdbvzqv6fxwa1lfsffxa3ma"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-atuin-client" ,rust-atuin-client-18)
                       ("rust-atuin-dotfiles" ,rust-atuin-dotfiles-18)
                       ("rust-atuin-history" ,rust-atuin-history-18)
                       ("rust-dashmap" ,rust-dashmap-5)
                       ("rust-eyre" ,rust-eyre-0.6)
                       ("rust-hyper-util" ,rust-hyper-util-0.1)
                       ("rust-listenfd" ,rust-listenfd-1)
                       ("rust-prost" ,rust-prost-0.13)
                       ("rust-prost-types" ,rust-prost-types-0.13)
                       ("rust-protox" ,rust-protox-0.7)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tokio-stream" ,rust-tokio-stream-0.1)
                       ("rust-tonic" ,rust-tonic-0.12)
                       ("rust-tonic-build" ,rust-tonic-build-0.12)
                       ("rust-tonic-types" ,rust-tonic-types-0.12)
                       ("rust-tower" ,rust-tower-0.4)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.3)
                       ("rust-uuid" ,rust-uuid-1))))
    (home-page "https://atuin.sh")
    (synopsis "The daemon crate for Atuin")
    (description "This package provides The daemon crate for Atuin.")
    (license license:expat)))

(define-public rust-tiny-bip39-1
  (package
    (name "rust-tiny-bip39")
    (version "1.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tiny-bip39" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0q98iv3wgbd41wyxxd5is8sddi53k9ary45rbi5fi8dmb39r9k32"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-anyhow" ,rust-anyhow-1)
                       ("rust-hmac" ,rust-hmac-0.12)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-pbkdf2" ,rust-pbkdf2-0.11)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rustc-hash" ,rust-rustc-hash-1)
                       ("rust-sha2" ,rust-sha2-0.10)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-unicode-normalization" ,rust-unicode-normalization-0.1)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
                       ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/maciejhirsz/tiny-bip39/")
    (synopsis
     "fork of the bip39 crate with fixes to v0.6. Rust implementation of BIP-0039")
    (description
     "This package provides a fork of the bip39 crate with fixes to v0.6.  Rust
implementation of BIP-0039.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sql-builder-3
  (package
    (name "rust-sql-builder")
    (version "3.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sql-builder" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1h5xp47zz9chv545lpmal51fq3z162z2f99mb4lhcbgcsaaqs05i"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-anyhow" ,rust-anyhow-1)
                       ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/perdumonocle/sql-builder.git")
    (synopsis "Simple SQL code generator")
    (description "This package provides Simple SQL code generator.")
    (license license:expat)))

(define-public rust-rusty-paseto-0.7
  (package
    (name "rust-rusty-paseto")
    (version "0.7.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rusty_paseto" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "09kqhfi2lnjhl9wjb26j6xg26k3w41i1ll3ardjw1ifali0ihl05"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-aes" ,rust-aes-0.7)
                       ("rust-base64" ,rust-base64-0.22)
                       ("rust-blake2" ,rust-blake2-0.10)
                       ("rust-chacha20" ,rust-chacha20-0.9)
                       ("rust-chacha20poly1305" ,rust-chacha20poly1305-0.10)
                       ("rust-digest" ,rust-digest-0.10)
                       ("rust-ed25519-dalek" ,rust-ed25519-dalek-2)
                       ("rust-erased-serde" ,rust-erased-serde-0.4)
                       ("rust-hex" ,rust-hex-0.4)
                       ("rust-hmac" ,rust-hmac-0.12)
                       ("rust-iso8601" ,rust-iso8601-0.6)
                       ("rust-p384" ,rust-p384-0.13)
                       ("rust-rand-core" ,rust-rand-core-0.6)
                       ("rust-ring" ,rust-ring-0.17)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-sha2" ,rust-sha2-0.10)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/rrrodzilla/rusty_paseto")
    (synopsis
     "type-driven, ergonomic alternative to JWT for secure stateless PASETO tokens.")
    (description
     "This package provides a type-driven, ergonomic alternative to JWT for secure
stateless PASETO tokens.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rusty-paserk-0.4
  (package
    (name "rust-rusty-paserk")
    (version "0.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rusty_paserk" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0f0xqrjbvx7mb2ynnqni9ql8qlg3zzn504vnyjmyh7ilrlgailx1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-aes" ,rust-aes-0.8)
                       ("rust-arbitrary" ,rust-arbitrary-1)
                       ("rust-argon2" ,rust-argon2-0.5)
                       ("rust-base64" ,rust-base64-0.22)
                       ("rust-base64ct" ,rust-base64ct-1)
                       ("rust-blake2" ,rust-blake2-0.10)
                       ("rust-chacha20" ,rust-chacha20-0.9)
                       ("rust-cipher" ,rust-cipher-0.4)
                       ("rust-ctr" ,rust-ctr-0.9)
                       ("rust-curve25519-dalek" ,rust-curve25519-dalek-4)
                       ("rust-digest" ,rust-digest-0.10)
                       ("rust-ed25519-dalek" ,rust-ed25519-dalek-2)
                       ("rust-generic-array" ,rust-generic-array-0.14)
                       ("rust-hmac" ,rust-hmac-0.12)
                       ("rust-p384" ,rust-p384-0.13)
                       ("rust-pbkdf2" ,rust-pbkdf2-0.12)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rusty-paseto" ,rust-rusty-paseto-0.7)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-sha2" ,rust-sha2-0.10)
                       ("rust-subtle" ,rust-subtle-2))))
    (home-page "https://github.com/conradludgate/rusty-paserk")
    (synopsis
     "Platform Agnostic Serializable Keys (PASERK) is an extension on PASETO for key management")
    (description
     "This package provides Platform Agnostic Serializable Keys (PASERK) is an extension on PASETO for key
management.")
    (license license:expat)))

(define-public rust-minspan-0.1
  (package
    (name "rust-minspan")
    (version "0.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "minspan" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0053r44iqmfilibz8da3367adxjjwibw6d849xifxq0yhfgf99pf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/mwotton/minspan")
    (synopsis
     "a package for determining the minimum span of one vector within another")
    (description
     "This package provides a package for determining the minimum span of one vector within another.")
    (license license:expat)))

(define-public rust-logos-codegen-0.14
  (package
    (name "rust-logos-codegen")
    (version "0.14.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "logos-codegen" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0gwnx7lk4y7xc4yk6pr0knrddard5z22rxaz9xrnc38cc1lh1y2r"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-beef" ,rust-beef-0.5)
                       ("rust-fnv" ,rust-fnv-1)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-regex-syntax" ,rust-regex-syntax-0.8)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://logos.maciej.codes/")
    (synopsis "Create ridiculously fast Lexers")
    (description "This package provides Create ridiculously fast Lexers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-logos-derive-0.14
  (package
    (name "rust-logos-derive")
    (version "0.14.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "logos-derive" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "07bk3q4jry9f8blrnsiy872ivilzy62xaglnn2ni5p590qmp5yr4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-logos-codegen" ,rust-logos-codegen-0.14))))
    (home-page "https://logos.maciej.codes/")
    (synopsis "Create ridiculously fast Lexers")
    (description "This package provides Create ridiculously fast Lexers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-logos-0.14
  (package
    (name "rust-logos")
    (version "0.14.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "logos" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0n349vin9mx326fkz68bsa4vc5sdn9n8qnfz7n1yqynbz1p3albj"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-logos-derive" ,rust-logos-derive-0.14))))
    (home-page "https://logos.maciej.codes/")
    (synopsis "Create ridiculously fast Lexers")
    (description "This package provides Create ridiculously fast Lexers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-interim-0.1
  (package
    (name "rust-interim")
    (version "0.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "interim" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1x5ykyv8bkv13398q3dpycg5943rw1jycvjbhi2yih30zw5hzzcs"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-chrono" ,rust-chrono-0.4)
                       ("rust-logos" ,rust-logos-0.14)
                       ("rust-time" ,rust-time-0.3))))
    (home-page "https://github.com/conradludgate/interim")
    (synopsis
     "parses simple English dates, inspired by Linux date command, and forked from chrono-english")
    (description
     "This package provides parses simple English dates, inspired by Linux date command, and forked from
chrono-english.")
    (license license:expat)))

(define-public rust-typed-builder-macro-0.18
  (package
    (name "rust-typed-builder-macro")
    (version "0.18.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typed-builder-macro" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0qwfq0q2lkg4bkmcpsqajy3ss2sb2h47dj5zhfwvbp27ygx8sw8z"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/idanarye/rust-typed-builder")
    (synopsis "Compile-time type-checked builder derive")
    (description
     "This package provides Compile-time type-checked builder derive.")
    (license (list license:expat license:asl2.0))))

(define-public rust-typed-builder-0.18
  (package
    (name "rust-typed-builder")
    (version "0.18.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typed-builder" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1p9s9p7f3mnylrzdqbxj73d9dw95syma6pnnyfp3ys801s49qwvp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-typed-builder-macro" ,rust-typed-builder-macro-0.18))))
    (home-page "https://github.com/idanarye/rust-typed-builder")
    (synopsis "Compile-time type-checked builder derive")
    (description
     "This package provides Compile-time type-checked builder derive.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sqlx-sqlite-0.8
  (package
    (name "rust-sqlx-sqlite")
    (version "0.8.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sqlx-sqlite" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0h05ca26g428h4337k4nm0ww75bcdkiqzp883m7fc92v78fsfp7q"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-atoi" ,rust-atoi-2)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-flume" ,rust-flume-0.11)
                       ("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-executor" ,rust-futures-executor-0.3)
                       ("rust-futures-intrusive" ,rust-futures-intrusive-0.5)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-libsqlite3-sys" ,rust-libsqlite3-sys-0.30)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-percent-encoding" ,rust-percent-encoding-2)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-urlencoded" ,rust-serde-urlencoded-0.7)
                       ("rust-sqlx-core" ,rust-sqlx-core-0.8)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-url" ,rust-url-2)
                       ("rust-uuid" ,rust-uuid-1))))
    (home-page "https://github.com/launchbadge/sqlx")
    (synopsis
     "SQLite driver implementation for SQLx. Not for direct use; see the `sqlx` crate for details")
    (description
     "This package provides SQLite driver implementation for SQLx.  Not for direct use; see the `sqlx` crate
for details.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sqlx-postgres-0.8
  (package
    (name "rust-sqlx-postgres")
    (version "0.8.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sqlx-postgres" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "04wnjl51kfx0qbfsfmhqdshpmw32vzz2p8dksmj6gvb3ydbqmff5"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-atoi" ,rust-atoi-2)
                       ("rust-base64" ,rust-base64-0.22)
                       ("rust-bigdecimal" ,rust-bigdecimal-0.4)
                       ("rust-bit-vec" ,rust-bit-vec-0.6)
                       ("rust-bitflags" ,rust-bitflags-2)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-crc" ,rust-crc-3)
                       ("rust-dotenvy" ,rust-dotenvy-0.15)
                       ("rust-etcetera" ,rust-etcetera-0.8)
                       ("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-hex" ,rust-hex-0.4)
                       ("rust-hkdf" ,rust-hkdf-0.12)
                       ("rust-hmac" ,rust-hmac-0.12)
                       ("rust-home" ,rust-home-0.5)
                       ("rust-ipnetwork" ,rust-ipnetwork-0.20)
                       ("rust-itoa" ,rust-itoa-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mac-address" ,rust-mac-address-1)
                       ("rust-md-5" ,rust-md-5-0.10)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-num-bigint" ,rust-num-bigint-0.4)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rust-decimal" ,rust-rust-decimal-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-sha2" ,rust-sha2-0.10)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-sqlx-core" ,rust-sqlx-core-0.8)
                       ("rust-stringprep" ,rust-stringprep-0.1)
                       ("rust-thiserror" ,rust-thiserror-2)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-uuid" ,rust-uuid-1)
                       ("rust-whoami" ,rust-whoami-1))))
    (home-page "https://github.com/launchbadge/sqlx")
    (synopsis
     "PostgreSQL driver implementation for SQLx. Not for direct use; see the `sqlx` crate for details")
    (description
     "This package provides @code{PostgreSQL} driver implementation for SQLx.  Not for direct use; see the
`sqlx` crate for details.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sqlx-mysql-0.8
  (package
    (name "rust-sqlx-mysql")
    (version "0.8.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sqlx-mysql" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0czjzzjm2y6lkhxvvzrzwgp0pmlhymcnym20hn9n9kh01s7jfq25"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-atoi" ,rust-atoi-2)
                       ("rust-base64" ,rust-base64-0.22)
                       ("rust-bigdecimal" ,rust-bigdecimal-0.4)
                       ("rust-bitflags" ,rust-bitflags-2)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-bytes" ,rust-bytes-1)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-crc" ,rust-crc-3)
                       ("rust-digest" ,rust-digest-0.10)
                       ("rust-dotenvy" ,rust-dotenvy-0.15)
                       ("rust-either" ,rust-either-1)
                       ("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-io" ,rust-futures-io-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-generic-array" ,rust-generic-array-0.14)
                       ("rust-hex" ,rust-hex-0.4)
                       ("rust-hkdf" ,rust-hkdf-0.12)
                       ("rust-hmac" ,rust-hmac-0.12)
                       ("rust-itoa" ,rust-itoa-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-md-5" ,rust-md-5-0.10)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-percent-encoding" ,rust-percent-encoding-2)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rsa" ,rust-rsa-0.9)
                       ("rust-rust-decimal" ,rust-rust-decimal-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-sha1" ,rust-sha1-0.10)
                       ("rust-sha2" ,rust-sha2-0.10)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-sqlx-core" ,rust-sqlx-core-0.8)
                       ("rust-stringprep" ,rust-stringprep-0.1)
                       ("rust-thiserror" ,rust-thiserror-2)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-uuid" ,rust-uuid-1)
                       ("rust-whoami" ,rust-whoami-1))))
    (home-page "https://github.com/launchbadge/sqlx")
    (synopsis
     "MySQL driver implementation for SQLx. Not for direct use; see the `sqlx` crate for details")
    (description
     "This package provides @code{MySQL} driver implementation for SQLx.  Not for direct use; see the `sqlx`
crate for details.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sqlx-macros-core-0.8
  (package
    (name "rust-sqlx-macros-core")
    (version "0.8.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sqlx-macros-core" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1bg7sn6l8dc4pzrqx2dwc3sp7dbn97msfqahpycnl55bqnn917sf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-async-std" ,rust-async-std-1)
                       ("rust-dotenvy" ,rust-dotenvy-0.15)
                       ("rust-either" ,rust-either-1)
                       ("rust-heck" ,rust-heck-0.5)
                       ("rust-hex" ,rust-hex-0.4)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-sha2" ,rust-sha2-0.10)
                       ("rust-sqlx-core" ,rust-sqlx-core-0.8)
                       ("rust-sqlx-mysql" ,rust-sqlx-mysql-0.8)
                       ("rust-sqlx-postgres" ,rust-sqlx-postgres-0.8)
                       ("rust-sqlx-sqlite" ,rust-sqlx-sqlite-0.8)
                       ("rust-syn" ,rust-syn-2)
                       ("rust-tempfile" ,rust-tempfile-3)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-url" ,rust-url-2))))
    (home-page "https://github.com/launchbadge/sqlx")
    (synopsis
     "Macro support core for SQLx, the Rust SQL toolkit. Not intended to be used directly")
    (description
     "This package provides Macro support core for SQLx, the Rust SQL toolkit.  Not intended to be used
directly.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sqlx-macros-0.8
  (package
    (name "rust-sqlx-macros")
    (version "0.8.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sqlx-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "047k67sylscv0gdhwwqrn0s33jy1mvq8rmqq6s8fygv4g2ny44ii"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-sqlx-core" ,rust-sqlx-core-0.8)
                       ("rust-sqlx-macros-core" ,rust-sqlx-macros-core-0.8)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/launchbadge/sqlx")
    (synopsis
     "Macros for SQLx, the rust SQL toolkit. Not intended to be used directly")
    (description
     "This package provides Macros for SQLx, the rust SQL toolkit.  Not intended to be used directly.")
    (license (list license:expat license:asl2.0))))

(define-public rust-hashlink-0.10
  (package
    (name "rust-hashlink")
    (version "0.10.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "hashlink" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1h8lzvnl9qxi3zyagivzz2p1hp6shgddfmccyf6jv7s1cdicz0kk"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-hashbrown" ,rust-hashbrown-0.15)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/kyren/hashlink")
    (synopsis
     "HashMap-like containers that hold their key-value pairs in a user controllable order")
    (description
     "This package provides @code{HashMap-like} containers that hold their key-value pairs in a user
controllable order.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sqlx-core-0.8
  (package
    (name "rust-sqlx-core")
    (version "0.8.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sqlx-core" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1q31dawr61wc6q2f12my4fw082mbv8sxwz1082msjsk76rlpn03a"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-async-io" ,rust-async-io-1)
                       ("rust-async-std" ,rust-async-std-1)
                       ("rust-bigdecimal" ,rust-bigdecimal-0.4)
                       ("rust-bit-vec" ,rust-bit-vec-0.6)
                       ("rust-bstr" ,rust-bstr-1)
                       ("rust-bytes" ,rust-bytes-1)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-crc" ,rust-crc-3)
                       ("rust-crossbeam-queue" ,rust-crossbeam-queue-0.3)
                       ("rust-either" ,rust-either-1)
                       ("rust-event-listener" ,rust-event-listener-5)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-intrusive" ,rust-futures-intrusive-0.5)
                       ("rust-futures-io" ,rust-futures-io-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-hashbrown" ,rust-hashbrown-0.15)
                       ("rust-hashlink" ,rust-hashlink-0.10)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-ipnetwork" ,rust-ipnetwork-0.20)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mac-address" ,rust-mac-address-1)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-native-tls" ,rust-native-tls-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-percent-encoding" ,rust-percent-encoding-2)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-rust-decimal" ,rust-rust-decimal-1)
                       ("rust-rustls" ,rust-rustls-0.23)
                       ("rust-rustls-native-certs" ,rust-rustls-native-certs-0.8)
                       ("rust-rustls-pemfile" ,rust-rustls-pemfile-2)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-sha2" ,rust-sha2-0.10)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-thiserror" ,rust-thiserror-2)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tokio-stream" ,rust-tokio-stream-0.1)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-url" ,rust-url-2)
                       ("rust-uuid" ,rust-uuid-1)
                       ("rust-webpki-roots" ,rust-webpki-roots-0.26))))
    (home-page "https://github.com/launchbadge/sqlx")
    (synopsis
     "Core of SQLx, the rust SQL toolkit. Not intended to be used directly")
    (description
     "This package provides Core of SQLx, the rust SQL toolkit.  Not intended to be used directly.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sqlx-0.8
  (package
    (name "rust-sqlx")
    (version "0.8.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sqlx" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0pvlpq0plgyxf5kikcv786pf0pjv8dx5shlvz72l510d7hxyf424"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-sqlx-core" ,rust-sqlx-core-0.8)
                       ("rust-sqlx-macros" ,rust-sqlx-macros-0.8)
                       ("rust-sqlx-mysql" ,rust-sqlx-mysql-0.8)
                       ("rust-sqlx-postgres" ,rust-sqlx-postgres-0.8)
                       ("rust-sqlx-sqlite" ,rust-sqlx-sqlite-0.8))))
    (home-page "https://github.com/launchbadge/sqlx")
    (synopsis
     "ð§° The Rust SQL Toolkit. An async, pure Rust SQL crate featuring compile-time checked queries without a DSL. Supports PostgreSQL, MySQL, and SQLite")
    (description
     "This package provides ð§° The Rust SQL Toolkit.  An async, pure Rust SQL crate featuring compile-time
checked queries without a DSL. Supports @code{PostgreSQL}, @code{MySQL}, and
SQLite.")
    (license (list license:expat license:asl2.0))))

(define-public rust-atuin-common-18
  (package
    (name "rust-atuin-common")
    (version "18.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "atuin-common" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1l3hprwyvc95343l1ls0hqn8pixr5w9vnpshfbrb1n2p8yg5zzpw"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-base64" ,rust-base64-0.22)
                       ("rust-directories" ,rust-directories-5)
                       ("rust-eyre" ,rust-eyre-0.6)
                       ("rust-getrandom" ,rust-getrandom-0.2)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-semver" ,rust-semver-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-sqlx" ,rust-sqlx-0.8)
                       ("rust-sysinfo" ,rust-sysinfo-0.30)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-typed-builder" ,rust-typed-builder-0.18)
                       ("rust-uuid" ,rust-uuid-1))))
    (home-page "https://atuin.sh")
    (synopsis "common library for atuin")
    (description "This package provides common library for atuin.")
    (license license:expat)))

(define-public rust-atuin-client-18
  (package
    (name "rust-atuin-client")
    (version "18.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "atuin-client" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1ryg3cj1hayr774rswj7nk71bvjgkw91s80jc3finfk0sapi6pq1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-async-trait" ,rust-async-trait-0.1)
                       ("rust-atuin-common" ,rust-atuin-common-18)
                       ("rust-base64" ,rust-base64-0.22)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-config" ,rust-config-0.13)
                       ("rust-crossterm" ,rust-crossterm-0.27)
                       ("rust-crypto-secretbox" ,rust-crypto-secretbox-0.1)
                       ("rust-directories" ,rust-directories-5)
                       ("rust-eyre" ,rust-eyre-0.6)
                       ("rust-fs-err" ,rust-fs-err-2)
                       ("rust-futures" ,rust-futures-0.3)
                       ("rust-generic-array" ,rust-generic-array-0.14)
                       ("rust-hex" ,rust-hex-0.4)
                       ("rust-humantime" ,rust-humantime-2)
                       ("rust-indicatif" ,rust-indicatif-0.17)
                       ("rust-interim" ,rust-interim-0.1)
                       ("rust-itertools" ,rust-itertools-0.13)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-minspan" ,rust-minspan-0.1)
                       ("rust-palette" ,rust-palette-0.7)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-reqwest" ,rust-reqwest-0.11)
                       ("rust-rmp" ,rust-rmp-0.8)
                       ("rust-rusty-paserk" ,rust-rusty-paserk-0.4)
                       ("rust-rusty-paseto" ,rust-rusty-paseto-0.7)
                       ("rust-semver" ,rust-semver-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-serde-regex" ,rust-serde-regex-1)
                       ("rust-serde-with" ,rust-serde-with-3)
                       ("rust-sha2" ,rust-sha2-0.10)
                       ("rust-shellexpand" ,rust-shellexpand-3)
                       ("rust-sql-builder" ,rust-sql-builder-3)
                       ("rust-sqlx" ,rust-sqlx-0.8)
                       ("rust-strum" ,rust-strum-0.26)
                       ("rust-strum-macros" ,rust-strum-macros-0.26)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tiny-bip39" ,rust-tiny-bip39-1)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-typed-builder" ,rust-typed-builder-0.18)
                       ("rust-urlencoding" ,rust-urlencoding-2)
                       ("rust-uuid" ,rust-uuid-1)
                       ("rust-whoami" ,rust-whoami-1))))
    (home-page "https://atuin.sh")
    (synopsis "client library for atuin")
    (description "This package provides client library for atuin.")
    (license license:expat)))

(define-public rust-arboard-3
  (package
    (name "rust-arboard")
    (version "3.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "arboard" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1x2p8dfhzm3w0cpw81ab2rbyzvkzqs9g66xcakq4y0fd2v5rq2fz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-clipboard-win" ,rust-clipboard-win-5)
                       ("rust-core-graphics" ,rust-core-graphics-0.23)
                       ("rust-image" ,rust-image-0.25)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-objc2" ,rust-objc2-0.5)
                       ("rust-objc2-app-kit" ,rust-objc2-app-kit-0.2)
                       ("rust-objc2-foundation" ,rust-objc2-foundation-0.2)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-windows-sys" ,rust-windows-sys-0.48)
                       ("rust-wl-clipboard-rs" ,rust-wl-clipboard-rs-0.8)
                       ("rust-x11rb" ,rust-x11rb-0.13))))
    (home-page "https://github.com/1Password/arboard")
    (synopsis "Image and text handling for the OS clipboard")
    (description
     "This package provides Image and text handling for the OS clipboard.")
    (license (list license:expat license:asl2.0))))

(define-public rust-atuin-18
  (package
    (name "rust-atuin")
    (version "18.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "atuin" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0ylr51crjq55d4m3108bwl0ahlrwrda6m7mgc6xgcnjrj0j23cly"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-arboard" ,rust-arboard-3)
                       ("rust-async-trait" ,rust-async-trait-0.1)
                       ("rust-atuin-client" ,rust-atuin-client-18)
                       ("rust-atuin-common" ,rust-atuin-common-18)
                       ("rust-atuin-daemon" ,rust-atuin-daemon-18)
                       ("rust-atuin-dotfiles" ,rust-atuin-dotfiles-18)
                       ("rust-atuin-history" ,rust-atuin-history-18)
                       ("rust-atuin-server" ,rust-atuin-server-18)
                       ("rust-atuin-server-postgres" ,rust-atuin-server-postgres-18)
                       ("rust-base64" ,rust-base64-0.22)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-clap-complete" ,rust-clap-complete-4)
                       ("rust-clap-complete-nushell" ,rust-clap-complete-nushell-4)
                       ("rust-colored" ,rust-colored-2)
                       ("rust-crossterm" ,rust-crossterm-0.27)
                       ("rust-directories" ,rust-directories-5)
                       ("rust-env-logger" ,rust-env-logger-0.11)
                       ("rust-eyre" ,rust-eyre-0.6)
                       ("rust-fs-err" ,rust-fs-err-2)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-fuzzy-matcher" ,rust-fuzzy-matcher-0.3)
                       ("rust-indicatif" ,rust-indicatif-0.17)
                       ("rust-interim" ,rust-interim-0.1)
                       ("rust-itertools" ,rust-itertools-0.13)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-ratatui" ,rust-ratatui-0.27)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-rpassword" ,rust-rpassword-7)
                       ("rust-runtime-format" ,rust-runtime-format-0.1)
                       ("rust-rustix" ,rust-rustix-0.38)
                       ("rust-semver" ,rust-semver-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-sysinfo" ,rust-sysinfo-0.30)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tiny-bip39" ,rust-tiny-bip39-1)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.3)
                       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
                       ("rust-unicode-width" ,rust-unicode-width-0.1)
                       ("rust-uuid" ,rust-uuid-1)
                       ("rust-whoami" ,rust-whoami-1))
       #:cargo-development-inputs (("rust-tracing-tree" ,rust-tracing-tree-0.4))
       #:tests? #f))
    (home-page "https://atuin.sh")
    (synopsis "atuin - magical shell history")
    (description "This package provides atuin - magical shell history.")
    (license license:expat)))

