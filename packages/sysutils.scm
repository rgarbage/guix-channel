(define-module (packages sysutils)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (guix build gnu-build-system)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages haskell-apps)
  #:use-module (gnu packages perl))

(define-public lsb-release
  (package
    (name "lsb-release")
    (version "12.0")
    (home-page "https://salsa.debian.org/gioele/lsb-release-minimal")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://salsa.debian.org/gioele/lsb-release-minimal")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1b460f41vj18760fr2l6zz5fwrkbjds8w2zxw116yly4iapiwx11"))))
    (inputs (list shellcheck bats perl))
    (build-system gnu-build-system)
    (arguments
      (list
        #:phases #~(modify-phases %standard-phases
                                (add-after 'unpack 'fix-makefile
                                           (lambda _
                                             (substitute* "Makefile" (("prefix=(.*)") ""))))
                                (delete 'configure) ; no ./configure script
                                (delete 'build)     ; nothing to build
                                (delete 'check)     ; check needs bats > 1.5.0 not in guix
                                (add-after 'install 'fix-path
                                           (lambda _
                                             (wrap-program (string-append #$output "/bin/lsb_release")
                                                           `("PATH" ":" prefix (,(string-append #$util-linux "/bin:")))))))
        #:make-flags 
        #~(list (string-append "DESTDIR=" #$output))))
    (synopsis "Minimal version of @command{lsb_release}")
    (description "This repository contains a bare-bones version of the 
@command{lsb_release} command, implemented as a tiny POSIX shell script.")
    (license license:isc)))
