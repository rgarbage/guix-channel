(define-module (services home matterhorn)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix deprecation)
  #:use-module (guix diagnostics)
  #:use-module (guix i18n)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (guix modules)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu packages haskell)
  #:use-module (gnu packages haskell-apps)
  #:use-module (srfi srfi-9)
  #:autoload (guix packages)
  (package-name package-version)
  #:autoload (guix utils)
  (%current-system)
  #:export (token-command token-command?
                          token-command->value

                          password
                          password?
                          password->value

                          password-command
                          password-command?
                          password-command->value

                          home-matterhorn-configuration
                          home-matterhorn-configuration-files
                          home-matterhorn-emoji-file?
                          home-matterhorn-configuration-file
                          home-matterhorn-file-name
                          home-matterhorn-user
                          home-matterhorn-host
                          home-matterhorn-authentication
                          home-matterhorn-extra-content

                          home-matterhorn-service-type))

(define-record-type <password>
  (password val) password?
  (val password->value))

(define-record-type <token-command>
  (token-command command) token-command?
  (command token-command->value))

(define-record-type <password-command>
  (password-command command) password-command?
  (command password-command->value))

(define-maybe authentication)

(define (authentication? x)
  (or (password? x)
      (token-command? x)
      (password-command? x)))

(define-record-type* <home-matterhorn-configuration>
                     home-matterhorn-configuration
                     make-home-matterhorn-configuration
  home-matterhorn-configuration?
  (configuration-files home-matterhorn-configuration-files ;list of <home-matterhorn-configuration-file records
                       (default '()))
  (emoji-file? home-matterhorn-emoji-file? ;boolean
               (default #t)))

(define-record-type* <home-matterhorn-configuration-file>
                     home-matterhorn-configuration-file
                     make-home-matterhorn-configuration-file
  home-matterhorn-configuration-file?
  (file-name home-matterhorn-file-name ;string
             (default "config.ini"))
  (user home-matterhorn-user ;string
        (default #f))
  (host home-matterhorn-host ;string
        (default #f))
  (authentication home-matterhorn-authentication ;authentication
                  (default #f))
  (extra-content home-matterhorn-extra-content ;string
                 (default "")))

(define (matterhorn-configuration->gexp config)
  #~(call-with-output-file #$output
      (lambda (port)
        (format port
                "[mattermost]~%user: ~a~%host: ~a~%~a~%~a~%"
                #$(home-matterhorn-user config)
                #$(home-matterhorn-host config)
                #$(cond
                    ((password? (home-matterhorn-authentication config))
                     #~(string-append "password: "
                                      #$(password->value (home-matterhorn-authentication
                                                          config))))
                    ((token-command? (home-matterhorn-authentication config))
                     #~(string-append "tokencmd: "
                                      #$(token-command->value (home-matterhorn-authentication
                                                               config))))
                    ((password-command? (home-matterhorn-authentication config))
                     #~(string-append "passcmd: "
                                      #$(password-command->value (home-matterhorn-authentication
                                                               config)))))
                #$(home-matterhorn-extra-content config)))))

(define (config->file-list config)
  (map (lambda (entry)
         (list (string-append "matterhorn/"
                              (home-matterhorn-file-name entry))
               (computed-file "matterhorn-config.ini"
                              (matterhorn-configuration->gexp entry))))
       (home-matterhorn-configuration-files config)))

(define (matterhorn-xdg-files config)
  (let ((lst (config->file-list config)))
    (if (home-matterhorn-emoji-file? config)
        (cons* `("matterhorn/emoji.json" ,(file-append matterhorn
                                                       "/share/"
                                                       (%current-system)
                                                       "-ghc-"
                                                       (package-version ghc)
                                                       "/"
                                                       (package-name
                                                        matterhorn)
                                                       "-"
                                                       (package-version
                                                        matterhorn)
                                                       "/emoji/emoji.json"))
               lst) lst)))

(define home-matterhorn-service-type
  (service-type (name 'home-matterhorn)
                (extensions (list (service-extension
                                   home-xdg-configuration-files-service-type
                                   matterhorn-xdg-files)))
                (description "Configure @command{matterhorn} by providing a
@file{~/.config/matterhorn/config.ini} file.")))
